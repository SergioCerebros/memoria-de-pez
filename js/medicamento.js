(function () {
    
    function setMedicamento ( medicamento ) {
        if ( Modernizr.sessionstorage ) {
            sessionStorage.setItem("medicamento", medicamento);
        } else {
            console.error("Session Storage not suported");
            return false;
        }
        
    }
    
    /* Si estás en la pagina del formulario ler opción padrón  */
    if ($('input:radio[name=Tratamiento]').length) {
        var medicamento = $('input:radio[name=Tratamiento]:checked').val(); 
        setMedicamento ( medicamento );
    }
    
    /* Si el usuario cambia el medicamento actualizar SessionStorage */   
    $("input:radio[name=Tratamiento]").change(function() {
        var medicamento = $(this).val();
        setMedicamento ( medicamento );
    });
    
})();

function getMedicamento() {
    if ( Modernizr.sessionstorage ) {
        var medicamento = sessionStorage.getItem("medicamento");
        return medicamento;
    } else {
        console.error("Session Storage not suported");
        return false;
    }
}


