function selectPlayYoutubeVideo ( videoid ) {
    player.loadVideoById( videoid );
    player.playVideo();
}

$(document).ready(function(){
    
    if ( Modernizr.svg == false) {
        $(".greenlogo").prop("src", "recursos-teaser/images/greenpeace.png");
    }
    
    /* --- FORM --- */

    $("input").not("[type=submit]").jqBootstrapValidation();
    
    $("#TextoPoliticaPrivacidad").html('<a id="EnlacePoliticaPrivacidad" href="javascript:void(0);">Consultar politica de privacidad</a>');
    
    $("form").on("click", "#EnlacePoliticaPrivacidad", function(){
        $("#TextoPoliticaPrivacidad").html('En cumplimiento de la LO 15/1999 de Protección de Datos de Carácter Personal, GREENPEACE ESPAÑA le informa de que sus datos personales están incluidos en el fichero de socios de la asociación cuya finalidad es disponer de los datos necesarios para la gestión del pago de las cuotas e informarle, por medios electrónicos, llamadas telefónicas y envío de nuestra revista, de las campañas y actividades realizadas y promovidas por la organización. Asimismo y salvo que en 30 días nos manifieste su negativa, le haremos llegar periódicamente nuestra newsletter. Usted podrá revocar su consentimiento para mantenerle informado así como ejercitar sus derechos de acceso, rectificación, cancelación y oposición ante el responsable del fichero GREENPEACE ESPAÑA en la siguiente dirección: Calle San Bernardo 107, 1º, 28015 Madrid.');
        typeof ( ga ) == "function" && ga('send', 'event', 'Teaser', 'PoliticaPrivacidad');
    });
    
    /* --- VIDEO --- */
    
    $("video").on("click", function() {
        var x = $(this).get(0);
        x.play();
        typeof ga == "function" && ga('send', 'event', 'Teaser', 'PlayVideo', 'Video' );
        $("#msgerror").hide();
        
    });
    
    /* --- Intento de resolver IE --- */
    if ( $('html').is('.lt-ie9') ) {
        $("video").prop("preload", "none");
        $("video").prop("poster", "recursos-teaser/videos/teaser-poster.jpg");
        $("video").before('<p id="msgerror" style="color: #f00;">Haz click en la foto para ver el vídeo</p>');
    }
    
    /* --- SOCIAL SHARING EVENTS --- */
    
    $(".facebook").on("click", function() {
        typeof ga == "function" && ga('send', 'event', 'Teaser', 'SocialShare', 'Facebook' );    
    });

    $(".twitter").on("click", function() {
        typeof ga == "function" && ga('send', 'event', 'Teaser', 'SocialShare', 'Twitter' );    
    });

    $(".googleplus").on("click", function() {
        typeof ga == "function" && ga('send', 'event', 'Teaser', 'SocialShare', 'GooglePlus' );    
    });

    $(".linkedin").on("click", function() {
        typeof ga == "function" && ga('send', 'event', 'Teaser', 'SocialShare', 'Linkedin' );    
    });
    
});