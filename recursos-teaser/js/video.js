var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    videoId: 'fQad1QLartE',
    events: {
      'onStateChange': onPlayerStateChange
    },
    playerVars: {
      autohide: 1,
      showinfo: 0,
      rel: 0
    }
  });
}
  
function onPlayerStateChange(event) {
  
  if (event.data == 1) {
      if (ga) {
          ga('send', 'event', 'Teaser', 'PlayVideo', 'Youtube');
      }
  }
  
}